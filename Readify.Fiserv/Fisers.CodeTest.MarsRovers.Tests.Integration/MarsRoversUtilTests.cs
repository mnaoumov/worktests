﻿using System.Diagnostics;
using System.IO;
using System.Reflection;
using NUnit.Framework;

namespace Fisers.CodeTest.MarsRovers.Tests.Integration
{
    [TestFixture]
    public class MarsRoversUtilTests
    {
        const string IOFiles = "IOFiles";
        string _tempFilePath;
        Process _process;

        [SetUp]
        public void SetUp()
        {
            string assemblyPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string programPath = Path.Combine(assemblyPath, "Fisers.CodeTest.MarsRovers.exe");
            _tempFilePath = Path.GetTempFileName();
            var psi = new ProcessStartInfo
                      {
                          FileName = programPath,
                          Arguments = string.Format("--input \"{0}\" --output \"{1}\"", InputFilePath, _tempFilePath),
                          CreateNoWindow = true,
                          UseShellExecute = false
                      };
            _process = Process.Start(psi);
            _process.WaitForExit();
        }

        static string InputFilePath
        {
            get
            {
                string assemblyPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                return Path.Combine(assemblyPath, IOFiles, "Input1.txt");
            }
        }

        static string OutputFilePath
        {
            get
            {
                string assemblyPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                return Path.Combine(assemblyPath, IOFiles, "Output1.txt");
            }
        }

        [TearDown]
        public void TearDown()
        {
            File.Delete(_tempFilePath);
        }


        [Test]
        public void When_succeded_exit_code_is_0()
        {
            Assert.That(_process.ExitCode, Is.EqualTo(0));
        }
        
        [Test]
        public void When_succeded_output_file_has_expected_content()
        {
            Assert.That(File.ReadAllText(_tempFilePath), Is.EqualTo(File.ReadAllText(OutputFilePath)));
        }
    }
}
