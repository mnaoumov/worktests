﻿using NUnit.Framework;

namespace Fisers.CodeTest.MarsRovers.Tests
{
    [TestFixture]
    public class RoverTests
    {
        [Test]
        public void Constructor_when_rover_position_is_outside_of_plateau_throws_InvalidOperationException()
        {
            Assert.That(() => new Rover(new PlateauSize(10, 20), new RoverPosition(11, 10, RoverDirection.North)), Throws.InvalidOperationException);
        }

        [Test]
        public void ApplyInstruction_TurnLeft_changes_Position_Direction_properly()
        {
            var rover = new Rover(new PlateauSize(10, 20), new RoverPosition(5, 10, RoverDirection.North));
            rover.ApplyInstruction(RoverInstruction.TurnLeft);
            Assert.That(rover.Position.Direction, Is.EqualTo(RoverDirection.West));
        }

        [Test]
        public void ApplyInstruction_TurnRight_changes_Position_Direction_properly()
        {
            var rover = new Rover(new PlateauSize(10, 20), new RoverPosition(5, 10, RoverDirection.North));
            rover.ApplyInstruction(RoverInstruction.TurnRight);
            Assert.That(rover.Position.Direction, Is.EqualTo(RoverDirection.East));
        }


        [Test]
        public void ApplyInstruction_Move_changes_Position_coordinates_properly()
        {
            var rover = new Rover(new PlateauSize(10, 20), new RoverPosition(5, 10, RoverDirection.North));
            rover.ApplyInstruction(RoverInstruction.Move);
            Assert.That(rover.Position, Is.EqualTo(new RoverPosition(5, 11, RoverDirection.North)));
        }
        
        [Test]
        public void ApplyInstruction_Move_when_result_position_is_outside_of_plateau_throws_InvalidOperationException()
        {
            var rover = new Rover(new PlateauSize(10, 20), new RoverPosition(5, 20, RoverDirection.North));
            Assert.That(() => rover.ApplyInstruction(RoverInstruction.Move),Throws.InvalidOperationException);
        }
    }
}