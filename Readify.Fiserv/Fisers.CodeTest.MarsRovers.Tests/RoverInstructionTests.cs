using System;
using NUnit.Framework;

namespace Fisers.CodeTest.MarsRovers.Tests
{
    [TestFixture]
    public class RoverInstructionTests
    {
        [Test]
        public void Parse_when_string_has_correct_format_parses_rover_instructions_correctly()
        {
            var instructions = RoverInstructionHelper.Parse("LRMLRM");
            Assert.That(instructions,
                        Is.EqualTo(
                            new[] { RoverInstruction.TurnLeft, RoverInstruction.TurnRight, RoverInstruction.Move, RoverInstruction.TurnLeft, RoverInstruction.TurnRight, RoverInstruction.Move }));
        }

        [Test]
        public void Parse_when_string_is_null_throws_ArgumentNullException()
        {
            Assert.That(() => RoverInstructionHelper.Parse(null), Throws.TypeOf<ArgumentNullException>());
        }

        [Test]
        public void Parse_when_instruntion_letter_is_not_LRM_throws_ArgumentException()
        {
            Assert.That(() => RoverInstructionHelper.Parse("QAS"), Throws.ArgumentException);
        }
    }
}