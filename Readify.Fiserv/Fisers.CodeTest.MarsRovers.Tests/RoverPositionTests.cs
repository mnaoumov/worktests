﻿using System;
using NUnit.Framework;

namespace Fisers.CodeTest.MarsRovers.Tests
{
    [TestFixture]
    public class RoverPositionTests
    {
        [Test]
        public void Parse_when_string_has_correct_format_parses_rover_position_correctly()
        {
            RoverPosition roverPosition = RoverPosition.Parse("1 2 N");
            Assert.That(roverPosition,
                        Is.EqualTo(
                            new RoverPosition(x: 1, y: 2, direction: RoverDirection.North)));
        }

        [Test]
        public void Parse_when_string_is_null_throws_ArgumentNullException()
        {
            Assert.That(() => RoverPosition.Parse(null), Throws.TypeOf<ArgumentNullException>());
        }

        [Test]
        public void Parse_when_string_has_wrong_format_throws_ArgumentException()
        {
            Assert.That(() => RoverPosition.Parse("abc def S"), Throws.ArgumentException);
        }

        [Test]
        public void Parse_when_some_coordinate_is_negative_throws_ArgumentException()
        {
            Assert.That(() => RoverPosition.Parse("-3 5 N"), Throws.ArgumentException);
        }


        [Test]
        public void Parse_when_direction_letter_is_not_NSEW_throws_ArgumentException()
        {
            Assert.That(() => RoverPosition.Parse("3 5 Q"), Throws.ArgumentException);
        }

        [Test]
        public void ToString_produces_correct_string_representation()
        {
            var position = new RoverPosition(x: 1, y: 2, direction: RoverDirection.North);
            Assert.That(position.ToString(), Is.EqualTo("1 2 N"));
        }
    }
}
