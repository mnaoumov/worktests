﻿using System;
using NUnit.Framework;

namespace Fisers.CodeTest.MarsRovers.Tests
{
    [TestFixture]
    public class PlateauSizeTests
    {
        [Test]
        public void Parse_when_string_has_correct_format_parses_plateau_size_correctly()
        {
            PlateauSize plateauSize = PlateauSize.Parse("2 3");
            Assert.That(plateauSize, Is.EqualTo(new PlateauSize(2, 3)));
        }

        [Test]
        public void Parse_when_string_is_null_throws_ArgumentNullException()
        {
            Assert.That(() => PlateauSize.Parse(null), Throws.TypeOf<ArgumentNullException>());
        }

        [Test]
        public void Parse_when_string_has_wrong_format_throws_ArgumentException()
        {
            Assert.That(() => PlateauSize.Parse("1 2 3"), Throws.ArgumentException);
        }

        [Test]
        public void Parse_when_some_coordinate_is_negative_throws_ArgumentException()
        {
            Assert.That(() => PlateauSize.Parse("-3 -5"), Throws.ArgumentException);
        }
    }
}