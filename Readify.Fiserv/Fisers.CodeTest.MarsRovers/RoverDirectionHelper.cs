using System;

namespace Fisers.CodeTest.MarsRovers
{
    public static class RoverDirectionHelper
    {
        public static bool TryParse(string str, out RoverDirection direction)
        {
            switch (str)
            {
                case ShortDirectionStrings.N:
                    direction = RoverDirection.North;
                    return true;
                case ShortDirectionStrings.S:
                    direction = RoverDirection.South;
                    return true;
                case ShortDirectionStrings.E:
                    direction = RoverDirection.East;
                    return true;
                case ShortDirectionStrings.W:
                    direction = RoverDirection.West;
                    return true;
                default:
                    direction = RoverDirection.NotSet;
                    return false;
            }
        }

        public static RoverDirection[] AllDirections
        {
            get { return new[] { RoverDirection.North, RoverDirection.South, RoverDirection.East, RoverDirection.West }; }
        }

        public static string ToShortString(this RoverDirection direction)
        {
            switch (direction)
            {
                case RoverDirection.North:
                    return ShortDirectionStrings.N;
                case RoverDirection.South:
                    return ShortDirectionStrings.S;
                case RoverDirection.East:
                    return ShortDirectionStrings.E;
                case RoverDirection.West:
                    return ShortDirectionStrings.W;
                default:
                    throw new NotSupportedException(string.Format("RoverDirection {0} cannot be converted to short string.", direction));
            }
        }

        static class ShortDirectionStrings
        {
            public const string N = "N";
            public const string S = "S";
            public const string E = "E";
            public const string W = "W";
        }
    }
}