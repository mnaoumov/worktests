﻿using System;

namespace Fisers.CodeTest.MarsRovers
{
    public struct PlateauSize
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public PlateauSize(int width, int height)
            : this()
        {
            if (width < 0 || height < 0)
                throw new ArgumentException(string.Format("{0},{1} is not correct plateau size.", width, height));

            Width = width;
            Height = height;
        }

        public static PlateauSize Parse(string str)
        {
            if (str == null)
                throw new ArgumentNullException("str", "Plateau size string cannot be null.");

            string[] partsArray = str.Split(' ');

            int width;
            int height;
            if (partsArray.Length != 2 || !int.TryParse(partsArray[0], out width) || !int.TryParse(partsArray[1], out height))
                throw new ArgumentException(str, string.Format("Cannot parse plateau size string '{0}'.", str));

            var plateauSize = new PlateauSize(width, height);
            return plateauSize;
        }
    }
}