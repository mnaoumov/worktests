﻿using System;
using System.IO;
using Fisers.CodeTest.MarsRovers.ConsoleUtils;
using Mono.Options;

namespace Fisers.CodeTest.MarsRovers
{
    public class Program : ConsoleUtilBase
    {
        string _inputFilePath;
        string _outputFilePath;

        static void Main(string[] args)
        {
            var program = new Program
                          {
                              Arguments = args
                          };
            program.Run();
        }

        protected override string Title
        {
            get { return "Fisers Code Test - Mars Rovers. (c) Michael Naumov"; }
        }

        protected override OptionSet OptionSet
        {
            get
            {
                return new OptionSet
                       {
                           { "input=", "Input file {PATH}", v => _inputFilePath = v },
                           { "output=", "Output file {PATH}", v => _outputFilePath = v }
                       };
            }
        }

        protected override void RunInternal()
        {
            if (string.IsNullOrEmpty(_inputFilePath))
                throw new ArgumentException("--input parameter should be specified.");

            if (!File.Exists(_inputFilePath))
                throw new ArgumentException(string.Format("Input file '{0}' not exists.", _inputFilePath));

            if (string.IsNullOrEmpty(_outputFilePath))
                throw new ArgumentException("--output parameter should be specified.");

            using (var reader = new StreamReader(_inputFilePath))
            {
                using (var writer = new StreamWriter(_outputFilePath))
                {
                    var plateauSize = PlateauSize.Parse(reader.ReadLine());
                    while (!reader.EndOfStream)
                    {
                        var position = RoverPosition.Parse(reader.ReadLine());
                        var instructions = RoverInstructionHelper.Parse(reader.ReadLine());
                        var rover = new Rover(plateauSize, position);
                        
                        foreach (var instruction in instructions)
                            rover.ApplyInstruction(instruction);
                        
                        writer.WriteLine(rover.Position.ToString());
                    }
                }
            }

            Console.WriteLine(string.Format("Output file '{0}' sucessfully written.", _outputFilePath));
        }
    }
}
