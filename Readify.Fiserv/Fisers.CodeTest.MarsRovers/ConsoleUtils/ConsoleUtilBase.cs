﻿using System;
using System.IO;
using Mono.Options;

namespace Fisers.CodeTest.MarsRovers.ConsoleUtils
{
    public abstract class ConsoleUtilBase
    {
        protected abstract OptionSet OptionSet { get; }
        protected abstract void RunInternal();
        protected abstract string Title { get; }
        protected string[] Arguments { get; set; }

        protected void Run()
        {
            Console.WriteLine(Title);
            try
            {
                if (Parse(OptionSet, Arguments))
                    RunInternal();
            }
            catch (Exception e)
            {
                if (Environment.ExitCode == 0)
                    Environment.ExitCode = 1;

                Console.WriteLine(e);
            }
        }

        static bool Parse(OptionSet optionSet, string[] arguments)
        {
            bool showHelp = arguments.Length == 0;
            optionSet.Add("h|help|?", "Show Help", v => showHelp = v != null);
            optionSet.Parse(arguments);

            if (showHelp)
            {
                string programName = Path.GetFileName(Environment.GetCommandLineArgs()[0]);
                Console.WriteLine(String.Format("{0} <options>", programName));
                optionSet.WriteOptionDescriptions(Console.Out);
            }

            return !showHelp;
        }
    }
}