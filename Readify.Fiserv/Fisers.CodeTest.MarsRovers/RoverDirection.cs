namespace Fisers.CodeTest.MarsRovers
{
    public enum RoverDirection
    {
        NotSet = 0,
        North,
        South,
        East,
        West
    }
}