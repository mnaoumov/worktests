using System;
using System.Collections.Generic;

namespace Fisers.CodeTest.MarsRovers
{
    public static class RoverInstructionHelper
    {
        public static RoverInstruction[] Parse(string str)
        {
            if (str == null)
                throw new ArgumentNullException("str", "Rover instructions string cannot be null.");

            var instructions = new List<RoverInstruction>();
            foreach (char symbol in str)
            {
                switch (symbol)
                {
                    case 'L':
                        instructions.Add(RoverInstruction.TurnLeft);
                        break;
                    case 'R':
                        instructions.Add(RoverInstruction.TurnRight);
                        break;
                    case 'M':
                        instructions.Add(RoverInstruction.Move);
                        break;
                    default:
                        throw new ArgumentException(string.Format("Instruction {0} is not valid.", symbol), "str");
                }
            }

            return instructions.ToArray();
        }
    }
}