using System;
using System.Collections.Generic;
using System.Linq;

namespace Fisers.CodeTest.MarsRovers
{
    public class PropertyEqualityComparer<T> : IEqualityComparer<T>
    {
        readonly Func<T, object>[] _propertyFuncs;

        public PropertyEqualityComparer(params Func<T, object>[] propertyFuncs)
        {
            _propertyFuncs = propertyFuncs;
        }

        public bool Equals(T x, object y)
        {
            if (ReferenceEquals(x, y))
                return true;

            if (x == null || !(y is T))
                return false;

            return Equals(x, (T) y);
        }


        public bool Equals(T x, T y)
        {
            return _propertyFuncs.All(pf => Equals(pf(x), pf(y)));
        }

        /// <summary>
        /// Inspired by http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int GetHashCode(T obj)
        {
            const int primeNumber1 = 17;
            const int primeNumber2 = 23;

            unchecked
            {
                int hash = primeNumber1;
                foreach (Func<T, object> propertyFunc in _propertyFuncs)
                {
                    object propertyValue = propertyFunc(obj);
                    int propertyHash = propertyValue != null ? propertyValue.GetHashCode() : primeNumber1;
                    hash = hash * primeNumber2 + propertyHash;
                }

                return hash;
            }
        }
    }
}