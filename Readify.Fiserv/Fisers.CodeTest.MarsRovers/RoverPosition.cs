﻿using System;
using System.Linq;

namespace Fisers.CodeTest.MarsRovers
{
    public class RoverPosition
    {
        static readonly PropertyEqualityComparer<RoverPosition> _equalityComparer;
        public int X { get; private set; }
        public int Y { get; private set; }
        public RoverDirection Direction { get; private set; }

        public RoverPosition(int x, int y, RoverDirection direction)
        {
            if (x < 0 || y < 0)
                throw new ArgumentException(string.Format("{0},{1} is not correct rover position.", x, y));

            if (!RoverDirectionHelper.AllDirections.Contains(direction))
                throw new ArgumentException(string.Format("{0} is not correct rover direction.", direction), "direction");

            X = x;
            Y = y;
            Direction = direction;
        }

        public static RoverPosition Parse(string str)
        {
            if (str == null)
                throw new ArgumentNullException("str", "RoverPosition string cannot be null.");

            string[] partsArray = str.Split(' ');

            int x;
            int y;
            RoverDirection direction;
            if (partsArray.Length != 3 || !int.TryParse(partsArray[0], out x) || !int.TryParse(partsArray[1], out y) || !RoverDirectionHelper.TryParse(partsArray[2], out direction))
                throw new ArgumentException(string.Format("Cannot parse rover position string '{0}'.", str), "str");

            return new RoverPosition(x, y, direction);
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", X, Y, Direction.ToShortString());
        }

        public override bool Equals(object obj)
        {
            return _equalityComparer.Equals(this, obj);
        }

        public override int GetHashCode()
        {
            return _equalityComparer.GetHashCode(this);
        }

        static RoverPosition()
        {
            _equalityComparer = new PropertyEqualityComparer<RoverPosition>(x => x.X, x => x.Y, x => x.Direction);
        }

        public void TurnLeft()
        {
            switch (Direction)
            {
                case RoverDirection.North:
                    Direction = RoverDirection.West;
                    break;
                case RoverDirection.West:
                    Direction = RoverDirection.South;
                    break;
                case RoverDirection.South:
                    Direction = RoverDirection.East;
                    break;
                case RoverDirection.East:
                    Direction = RoverDirection.North;
                    break;
                default:
                    throw new NotSupportedException(string.Format("RoverDirection {0} is not supported by TurnLeft.", Direction));
            }
        }

        public void TurnRight()
        {
            switch (Direction)
            {
                case RoverDirection.North:
                    Direction = RoverDirection.East;
                    break;
                case RoverDirection.East:
                    Direction = RoverDirection.South;
                    break;
                case RoverDirection.South:
                    Direction = RoverDirection.West;
                    break;
                case RoverDirection.West:
                    Direction = RoverDirection.North;
                    break;
                default:
                    throw new NotSupportedException(string.Format("RoverDirection {0} is not supported by TurnRight.", Direction));
            }
        }

        public void Move()
        {
            switch (Direction)
            {
                case RoverDirection.North:
                    Y++;
                    break;
                case RoverDirection.South:
                    Y--;
                    break;
                case RoverDirection.West:
                    X--;
                    break;
                case RoverDirection.East:
                    X++;
                    break;
                default:
                    throw new NotSupportedException(string.Format("Cannot apply instruction for direction {0}.", Direction));
            }
        }
    }
}