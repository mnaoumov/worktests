using System;

namespace Fisers.CodeTest.MarsRovers
{
    public class Rover
    {
        readonly PlateauSize _plateauSize;

        public Rover(PlateauSize plateauSize, RoverPosition position)
        {
            _plateauSize = plateauSize;
            Position = position;
            CheckPosition();
        }

        public RoverPosition Position { get; set; }

        void CheckPosition()
        {
            if (Position.X > _plateauSize.Width || Position.Y > _plateauSize.Height)
                throw new InvalidOperationException("Cannot move rover to the position {0} because it is outside plateau boundaries.");
        }

        public void ApplyInstruction(RoverInstruction instruction)
        {
            switch (instruction)
            {
                case RoverInstruction.TurnLeft:
                    Position.TurnLeft();
                    break;
                case RoverInstruction.TurnRight:
                    Position.TurnRight();
                    break;
                case RoverInstruction.Move:
                    Position.Move();
                    CheckPosition();
                    break;
                default:
                    throw new NotSupportedException(string.Format("Unknown instruction {0}.", instruction));
            }
        }
    }
}