namespace Fisers.CodeTest.MarsRovers
{
    public enum RoverInstruction
    {
        TurnLeft,
        TurnRight,
        Move
    }
}