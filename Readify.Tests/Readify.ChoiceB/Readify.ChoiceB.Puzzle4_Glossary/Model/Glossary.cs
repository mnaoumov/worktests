﻿namespace Readify.ChoiceB.Puzzle4_Glossary.Model
{
    public class Glossary
    {
        public long ID { get; set; }
        public string Term { get; set; }
        public string Definition { get; set; }

        public Glossary()
        {
            Term = "";
            Definition = "";
        }
    }
}