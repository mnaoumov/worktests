using System.ComponentModel;
using System.Windows;
using Readify.ChoiceB.Puzzle4_Glossary.DAL;
using Readify.ChoiceB.Puzzle4_Glossary.ViewModel;

namespace Readify.ChoiceB.Puzzle4_Glossary
{
    public class ViewModelLocator
    {
        static readonly bool _isInDesignMode;

        static ViewModelLocator()
        {
            _isInDesignMode = DesignerProperties.GetIsInDesignMode(new DependencyObject());
        }

        public MainViewModel MainViewModel
        {
            get
            {
                var unitOfWork = CreateUnitOfWork();
                return new MainViewModel(unitOfWork);
            }
        }

        static IUnitOfWork CreateUnitOfWork()
        {
            if (_isInDesignMode)
                return new DesignModeUnitOfWork();
            return new SqlUnitOfWork();
        }
    }
}