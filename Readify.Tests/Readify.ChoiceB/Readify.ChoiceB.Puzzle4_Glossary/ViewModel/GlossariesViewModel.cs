using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq.Expressions;
using Readify.ChoiceB.Puzzle4_Glossary.DAL;
using Readify.ChoiceB.Puzzle4_Glossary.Model;

namespace Readify.ChoiceB.Puzzle4_Glossary.ViewModel
{
    public class GlossariesViewModel : INotifyPropertyChanged
    {
        readonly IUnitOfWork _unitOfWork;

        public GlossariesViewModel(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _unitOfWork.Reloaded += _unitOfWork_Reloaded;
            SaveCommand = new DelegateCommand(Save);
            ReloadCommand = new DelegateCommand(Reload);
        }

        void _unitOfWork_Reloaded(object sender, EventArgs e)
        {
            RaisePropertyChanged(() => Items);
        }

        public ObservableCollection<Glossary> Items
        {
            get { return _unitOfWork.Glossaries.Get(); }
        }

        public DelegateCommand SaveCommand { get; private set; }
        public DelegateCommand ReloadCommand { get; private set; }

        void Reload()
        {
            _unitOfWork.Rollback();
        }

        void Save()
        {
            _unitOfWork.Commit();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void RaisePropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            var memberExpression = (MemberExpression) propertyExpression.Body;
            var propertyName = memberExpression.Member.Name;

            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}