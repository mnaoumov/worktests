﻿using Readify.ChoiceB.Puzzle4_Glossary.DAL;
using Readify.ChoiceB.Puzzle4_Glossary.Model;

namespace Readify.ChoiceB.Puzzle4_Glossary.ViewModel
{
    public class MainViewModel
    {
        public MainViewModel(IUnitOfWork unitOfWork)
        {
            Glossaries = new GlossariesViewModel(unitOfWork);
        }

        public GlossariesViewModel Glossaries { get; set; }
    }
}