using System.Collections.ObjectModel;

namespace Readify.ChoiceB.Puzzle4_Glossary.DAL
{
    public class DesignModeRepository<T> : IRepository<T>
    {
        readonly ObservableCollection<T> _items;

        public DesignModeRepository(T[] items)
        {
            _items = new ObservableCollection<T>(items);
        }

        public ObservableCollection<T> Get()
        {
            return _items;
        }
    }
}