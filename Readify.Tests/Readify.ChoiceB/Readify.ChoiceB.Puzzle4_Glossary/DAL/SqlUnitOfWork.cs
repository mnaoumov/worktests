using System;
using Readify.ChoiceB.Puzzle4_Glossary.Model;

namespace Readify.ChoiceB.Puzzle4_Glossary.DAL
{
    public class SqlUnitOfWork : IUnitOfWork
    {
        EntityContext _context;
        IRepository<Glossary> _glossaries;

        public SqlUnitOfWork()
        {
            Reload();
        }

        public void Rollback()
        {
            Reload();
        }

        void Reload()
        {
            if (_context != null)
                _context.Dispose();
            _context = new EntityContext();
            _glossaries = new SqlRepository<Glossary>(_context.Glossaries);
            OnReloaded(EventArgs.Empty);
        }

        public event EventHandler Reloaded;

        void OnReloaded(EventArgs eventArgs)
        {
            EventHandler handler = Reloaded;

            if (handler != null)
                handler(this, eventArgs);

        }

        public void Commit()
        {
            _context.SaveChanges();
            Reload();
        }

        public IRepository<Glossary> Glossaries
        {
            get { return _glossaries; }
        }
    }
}