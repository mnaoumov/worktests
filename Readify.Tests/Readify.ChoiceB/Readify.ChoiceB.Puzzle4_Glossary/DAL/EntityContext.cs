﻿using System.Collections.ObjectModel;
using System.Data.Entity;
using Readify.ChoiceB.Puzzle4_Glossary.Model;

namespace Readify.ChoiceB.Puzzle4_Glossary.DAL
{
    public class EntityContext : DbContext
    {
        static EntityContext()
        {
            Database.SetInitializer(new DatabaseInitializer());
        }
        public DbSet<Glossary> Glossaries { get; set; }
    }
}