using System;
using Readify.ChoiceB.Puzzle4_Glossary.Model;

namespace Readify.ChoiceB.Puzzle4_Glossary.DAL
{
    public class DesignModeUnitOfWork : IUnitOfWork
    {
        public DesignModeUnitOfWork()
        {
            Glossaries = new DesignModeRepository<Glossary>(new[]
                                                                 {
                                                                     new Glossary
                                                                         {
                                                                             Term = "abyssal plain",
                                                                             Definition = "The ocean floor offshore from the continental margin, usually very flat with a slight slope."
                                                                         },
                                                                     new Glossary
                                                                         {
                                                                             Term = "accrete",
                                                                             Definition = "v. To add terranes (small land masses or pieces of crust) to another, usually larger, land mass."
                                                                         },
                                                                     new Glossary
                                                                         {
                                                                             Term = "alkaline",
                                                                             Definition = "Term pertaining to a highly basic, as opposed to acidic, subtance. For example, hydroxide or carbonate of sodium or potassium."
                                                                         }
                                                                 });
        }

        public void Rollback()
        {
            throw new NotImplementedException();
        }

        public void Commit()
        {
            throw new NotImplementedException();
        }

        public IRepository<Glossary> Glossaries { get; private set; }
        public event EventHandler Reloaded;
    }
}