using System.Data.Entity;
using System.Data.SQLite;

namespace Readify.ChoiceB.Puzzle4_Glossary.DAL
{
    public class DatabaseInitializer : IDatabaseInitializer<EntityContext>
    {
        public void InitializeDatabase(EntityContext context)
        {
            var connection = (SQLiteConnection)context.Database.Connection;
            connection.Open();

            var cmd = new SQLiteCommand
                          {
                              Connection = connection,
                              CommandText = "select exists(select * from sqlite_master where tbl_name = \'Glossaries\')"
                          };

            var glossariesTableExists = SQLiteConvert.ToBoolean(cmd.ExecuteScalar());

            if (!glossariesTableExists)
            {
                cmd = new SQLiteCommand
                          {
                              Connection = connection,
                              CommandText = @"
create table Glossaries
(
    ID integer primary key autoincrement,
    Term nvarchar(50) not null,
    Definition nvarchar(65536) not null
)
"
                          };

                cmd.ExecuteNonQuery();

                var glossaries = new DesignModeUnitOfWork().Glossaries.Get();
                foreach (var glossary in glossaries)
                    context.Glossaries.Add(glossary);

                context.SaveChanges();
            }
        }
    }
}