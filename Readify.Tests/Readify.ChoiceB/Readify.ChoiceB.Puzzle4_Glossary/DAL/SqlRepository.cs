using System.Collections.ObjectModel;
using System.Data.Entity;

namespace Readify.ChoiceB.Puzzle4_Glossary.DAL
{
    public class SqlRepository<T> : IRepository<T> where T : class
    {
        readonly DbSet<T> _dbSet;

        public SqlRepository(DbSet<T> dbSet)
        {
            _dbSet = dbSet;
        }

        public ObservableCollection<T> Get()
        {
            _dbSet.Load();
            return _dbSet.Local;
        }
    }
}