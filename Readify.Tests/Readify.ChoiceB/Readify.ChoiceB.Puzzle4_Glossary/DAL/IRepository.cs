using System.Collections.ObjectModel;

namespace Readify.ChoiceB.Puzzle4_Glossary.DAL
{
    public interface IRepository<T>
    {
        ObservableCollection<T> Get();
    }
}