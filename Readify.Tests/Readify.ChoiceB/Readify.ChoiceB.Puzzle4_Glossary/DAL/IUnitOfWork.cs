using System;
using Readify.ChoiceB.Puzzle4_Glossary.Model;

namespace Readify.ChoiceB.Puzzle4_Glossary.DAL
{
    public interface IUnitOfWork
    {
        void Rollback();
        void Commit();
        IRepository<Glossary> Glossaries { get; }
        event EventHandler Reloaded;
    }
}