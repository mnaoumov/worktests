using System;
using System.Windows.Input;

namespace Readify.ChoiceB.Puzzle4_Glossary
{
    public class DelegateCommand : ICommand
    {
        readonly Action _action;

        public DelegateCommand(Action action)
        {
            _action = action;
        }

        public void Execute(object parameter)
        {
            _action();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;
    }
}