﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Moq;
using NUnit.Framework;
using Readify.ChoiceB.Puzzle4_Glossary.DAL;
using Readify.ChoiceB.Puzzle4_Glossary.Model;
using Readify.ChoiceB.Puzzle4_Glossary.Tests.Helper;
using Readify.ChoiceB.Puzzle4_Glossary.ViewModel;

namespace Readify.ChoiceB.Puzzle4_Glossary.Tests
{
    [TestFixture]
    public class GlossariesViewModel_Tests
    {
        Mock<IUnitOfWork> _mockUnitOfWork;
        ObservableCollection<Glossary> _glossaries;
        GlossariesViewModel _glossariesViewModel;

        [SetUp]
        public void SetUp()
        {
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            var mockGlossariesRepository = new Mock<IRepository<Glossary>>();
            _mockUnitOfWork.Setup(x => x.Glossaries).Returns(mockGlossariesRepository.Object);
            _glossaries = new ObservableCollection<Glossary>
                              {
                                  new Glossary
                                      {
                                          Term = "Term 1",
                                          Definition = "Definition 1"
                                      },
                                  new Glossary
                                      {
                                          Term = "Term 2",
                                          Definition = "Definition 2"
                                      }
                              };
            mockGlossariesRepository.Setup(x => x.Get()).Returns(_glossaries);
            _glossariesViewModel = new GlossariesViewModel(_mockUnitOfWork.Object);
        }

        [Test]
        public void Items_property_returns_Glossaries_collection_from_repository()
        {
            Assert.That(_glossariesViewModel.Items, Is.EqualTo(_glossaries));
        }

        [Test]
        public void SaveCommand_invokes_unit_of_work_Commit_method()
        {
            _glossariesViewModel.SaveCommand.Execute(null);
            _mockUnitOfWork.Verify(x => x.Commit());
        }

        [Test]
        public void ReloadCommand_invokes_unit_of_work_Rollback_method()
        {
            _glossariesViewModel.ReloadCommand.Execute(null);
            _mockUnitOfWork.Verify(x => x.Rollback());
        }

        [Test]
        public void When_unit_of_work_is_reloaded_ViewModel_notifies_about_its_Items_property_changes()
        {
            var mockPropertyChangesHandler = new Mock<ActionWrapper<object, PropertyChangedEventArgs>>();
            _glossariesViewModel.PropertyChanged += new PropertyChangedEventHandler(mockPropertyChangesHandler.Object.Delegate);
            _mockUnitOfWork.Raise(x => x.Reloaded += null, EventArgs.Empty);
            mockPropertyChangesHandler.Verify(x => x.Invoke(_glossariesViewModel, It.Is<PropertyChangedEventArgs>(args => args.PropertyName == "Items")));
        }

    }
}