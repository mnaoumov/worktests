using System;

namespace Readify.ChoiceB.Puzzle4_Glossary.Tests.Helper
{
    public abstract class ActionWrapper<T1, T2>
    {
        public Action<T1, T2> Delegate
        {
            get { return Invoke; }
        }

        public abstract void Invoke(T1 arg1, T2 arg2);
    }
}