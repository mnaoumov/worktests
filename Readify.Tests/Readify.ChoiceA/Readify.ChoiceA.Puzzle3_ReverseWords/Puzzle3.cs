using System.Text;

namespace Readify.ChoiceA.Puzzle3_ReverseWords
{
    public static class Puzzle3
    {
        public static string ReverseWords(string source)
        {
            if (source == null)
                return null;

            var result = new StringBuilder();
            var word = new StringBuilder();

            foreach (var symbol in source)
            {
                if (!char.IsWhiteSpace(symbol))
                    word.Insert(0, symbol);
                else
                {
                    result.Append(word);
                    result.Append(symbol);
                    word.Clear();
                }
            }

            result.Append(word);

            return result.ToString();
        }
    }
}