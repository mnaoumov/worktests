﻿using System;
using NUnit.Framework;

namespace Readify.ChoiceA.Puzzle1_LinkedList.Tests
{
    [TestFixture]
    public class Puzzle1_Get5thTailElement_Tests
    {
        [Test]
        public void When_list_contains_exactly_5_elements_returns_first_element()
        {
            LinkedList<int> list = new LinkedList<int>(1, 2, 3, 4, 5);
            Assert.That(Puzzle1.Get5thTailElement(list), Is.EqualTo(1));
        }

        [Test]
        public void When_list_contains_more_than_5_elements_returns_5th_element_from_tail()
        {
            var list = new LinkedList<int>(2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            Assert.That(Puzzle1.Get5thTailElement(list), Is.EqualTo(7));
        }
 
        [Test]
        public void When_list_is_null_throws_ArgumentNullException()
        {
            LinkedList<int> list = null;
            Assert.That(() => Puzzle1.Get5thTailElement(list), Throws.TypeOf<ArgumentNullException>());
        }

        [Test]
        public void When_list_contains_less_than_5_elements_throws_InvalidOperationException()
        {
            LinkedList<int> list = new LinkedList<int>(1, 2, 3, 4);
            Assert.That(() => Puzzle1.Get5thTailElement(list), Throws.TypeOf<InvalidOperationException>());
        }

        [Test]
        public void When_list_is_empty_throws_InvalidOperationException()
        {
            LinkedList<int> list = new LinkedList<int>();
            Assert.That(() => Puzzle1.Get5thTailElement(list), Throws.TypeOf<InvalidOperationException>());
        }
   }
}