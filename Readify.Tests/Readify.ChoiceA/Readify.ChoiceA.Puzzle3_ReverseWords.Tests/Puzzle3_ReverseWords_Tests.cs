﻿using NUnit.Framework;

namespace Readify.ChoiceA.Puzzle3_ReverseWords.Tests
{
    [TestFixture]
    public class Puzzle3_ReverseWords_Tests
    {
        [Test]
        public void When_source_string_is_null_returns_null()
        {
            var str = Puzzle3.ReverseWords(null);
            Assert.That(str, Is.Null);
        }

        [Test]
        public void When_source_has_one_letter_returns_this_letter()
        {
            var str = Puzzle3.ReverseWords("a");
            Assert.That(str, Is.EqualTo("a"));
        }

        [Test]
        public void When_source_has_one_word_returns_reversed_word()
        {
            var str = Puzzle3.ReverseWords("dog");
            Assert.That(str, Is.EqualTo("god"));
        }

        [Test]
        public void When_source_has_one_word_with_leading_whitespace_returns_reversed_word_with_leading_whitespace()
        {
            var str = Puzzle3.ReverseWords("\tdog");
            Assert.That(str, Is.EqualTo("\tgod"));
        }

        [Test]
        public void When_source_has_one_word_with_trailing_whitespace_returns_reversed_word_with_trailing_whitespace()
        {
            var str = Puzzle3.ReverseWords("dog   ");
            Assert.That(str, Is.EqualTo("god   "));
        }

        [Test]
        public void When_source_has_several_words_returns_reversed_words_keeping_whitespaces()
        {
            var str = Puzzle3.ReverseWords("Cat and dog");
            Assert.That(str, Is.EqualTo("taC dna god"));
        }

        [Test]
        public void Punctuation_characters_treated_as_part_of_words()
        {
            var str = Puzzle3.ReverseWords("Cat! and? dog;");
            Assert.That(str, Is.EqualTo("!taC ?dna ;god"));
        }
    }
}
