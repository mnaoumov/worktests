﻿using System;

namespace Readify.ChoiceA.Puzzle1_LinkedList
{
    public static class Puzzle1
    {
        public static int Get5thTailElement(LinkedList<int> list)
        {
            if (list == null)
                throw new ArgumentNullException("list");
            return list.GetNthTailElement(5);
        }
    }
}