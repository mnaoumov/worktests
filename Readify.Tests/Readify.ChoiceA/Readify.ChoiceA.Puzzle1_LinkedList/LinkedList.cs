using System;

namespace Readify.ChoiceA.Puzzle1_LinkedList
{
    public class LinkedList<T>
    {
        ListItem _head;

        public LinkedList(params T[] elements)
        {
            if (elements == null)
                throw new ArgumentNullException("elements");

            ListItem lastItem = null;
            foreach (var element in elements)
            {
                var nextItem = new ListItem(element);

                if (lastItem == null)
                    _head = nextItem;
                else
                    lastItem.Next = nextItem;

                lastItem = nextItem;
            }
        }

        class ListItem
        {
            public T Value { get; set; }
            public ListItem Next { get; set; }

            public ListItem(T value)
            {
                Value = value;
            }
        }

        public T GetNthTailElement(int n)
        {
            if (n <= 0)
                throw new ArgumentException("N should be greater than 0.", "n");

            var buffer = new T[n];
            var item = _head;
            for (int i = 0; i < n; i++)
            {
                if (item == null)
                    throw new InvalidOperationException(string.Format("List contains less than {0} items.", n));
                buffer[i] = item.Value;
                item = item.Next;
            }

            while (item != null)
            {
                for (int i = 0; i < n - 1; i++)
                    buffer[i] = buffer[i + 1];
                buffer[n - 1] = item.Value;
                item = item.Next;
            }

            return buffer[0];
        }
    }
}