namespace Readify.ChoiceA.Puzzle2_Triangles
{
    public class Triangle
    {
        readonly int _length1;
        readonly int _length2;
        readonly int _length3;

        public Triangle(int length1, int length2, int length3)
        {
            _length1 = length1;
            _length2 = length2;
            _length3 = length3;
        }

        public TriangleType GetTriangleType()
        {
            if (!CheckForPositiveLengths())
                return TriangleType.Error;

            if (!CheckForTriangularInequality())
                return TriangleType.Error;

            if (HasThreeEqualSides())
                return TriangleType.Equilateral;

            if (HasTwoEqualSides())
                return TriangleType.Isosceles;

            return TriangleType.Scalene;
        }

        bool HasTwoEqualSides()
        {
            return _length1 == _length2 || _length2 == _length3 || _length3 == _length1;
        }

        bool HasThreeEqualSides()
        {
            return _length1 == _length2 && _length2 == _length3;
        }

        bool CheckForTriangularInequality()
        {
            return _length1 < _length2 + _length3 && _length2 < _length3 + _length1 && _length3 < _length1 + _length2;
        }

        bool CheckForPositiveLengths()
        {
            return _length1 > 0 && _length2 > 0 && _length3 > 0;
        }
    }
}