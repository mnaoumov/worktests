namespace Readify.ChoiceA.Puzzle2_Triangles
{
    public static class Puzzle2
    {
        public static TriangleType GetTriangleType(int length1, int length2, int length3)
        {
            var triangle = new Triangle(length1, length2, length3);
            return triangle.GetTriangleType();
        }
    }
}