namespace Readify.ChoiceA.Puzzle2_Triangles
{
    public enum TriangleType
    {
        Scalene,
        Isosceles,
        Equilateral,
        Error
    }
}