﻿using NUnit.Framework;

namespace Readify.ChoiceA.Puzzle2_Triangles.Tests
{
    [TestFixture]
    public class Puzzle2_GetTriangleType_Tests
    {
        [Test]
        public void When_triangle_has_no_equal_sided_returns_type_Scalene()
        {
            var triangleType = Puzzle2.GetTriangleType(3, 4, 5);
            Assert.That(triangleType, Is.EqualTo(TriangleType.Scalene));
        }

        [Test]
        public void When_triangle_has_sides_with_negative_length_returns_type_Error()
        {
            var triangleType = Puzzle2.GetTriangleType(3, 4, -5);
            Assert.That(triangleType, Is.EqualTo(TriangleType.Error));
        }

        [Test]
        public void When_triangle_has_sides_with_zero_length_returns_type_Error()
        {
            var triangleType = Puzzle2.GetTriangleType(3, 4, 0);
            Assert.That(triangleType, Is.EqualTo(TriangleType.Error));
        }

        [Test]
        public void When_triangle_does_not_satisfy_triangular_inequility_returns_type_Error()
        {
            var triangleType = Puzzle2.GetTriangleType(3, 4, 8);
            Assert.That(triangleType, Is.EqualTo(TriangleType.Error));
        }

        [Test]
        public void When_triangle_has_three_equal_sides_returns_type_Equilateral()
        {
            var triangleType = Puzzle2.GetTriangleType(3, 3, 3);
            Assert.That(triangleType, Is.EqualTo(TriangleType.Equilateral));
        }

        [Test]
        public void When_triangle_has_two_equal_sides_returns_type_Isosceles()
        {
            var triangleType = Puzzle2.GetTriangleType(3, 3, 4);
            Assert.That(triangleType, Is.EqualTo(TriangleType.Isosceles));
        }
    }
}