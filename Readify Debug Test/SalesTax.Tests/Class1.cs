﻿using NUnit.Framework;

namespace SalesTax.Tests
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void T()
        {
            string inputLine = "1 book at 12.49";
            var saleLine = InputParser.ProcessInput(inputLine);

            Assert.That(saleLine.ProductName, Is.EqualTo("book"));
        }
    }
}
