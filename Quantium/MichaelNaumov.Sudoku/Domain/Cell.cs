using System.Collections.Generic;
using System.Linq;

namespace MichaelNaumov.SudokuTest.Domain
{
    public class Cell : SudokuObjectBase
    {
        public Row Row { get; set; }
        public Column Column { get; set; }
        public Group Group { get; set; }

        public int? Value { get; set; }
        public bool ValueWasGiven { get; set; }

        public override string ToString()
        {
            return Value == null ? "." : Value.ToString();
        }

        public override void PrepareCandidates()
        {
            Candidates = new HashSet<int>(Row.Candidates.Intersect(Column.Candidates).Intersect(Group.Candidates));
        }

        public void SetCalculatedValue(int calculatedValue)
        {
            Value = calculatedValue;

            foreach (var block in Blocks)
                block.PrepareCandidates();

            foreach (var cell in Blocks.SelectMany(b => b.GetUnsolvedCells()))
                cell.PrepareCandidates();
        }

        IEnumerable<Block> Blocks
        {
            get
            {
                yield return Row;
                yield return Column;
                yield return Group;
            }
        }

        public void Load(Cell cell)
        {
            Value = cell.Value;
            ValueWasGiven = cell.ValueWasGiven;
            Candidates = new HashSet<int>(cell.Candidates);
        }

        public Cell()
        {
            Candidates = new HashSet<int>();
        }
    }
}