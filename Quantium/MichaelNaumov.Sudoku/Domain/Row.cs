namespace MichaelNaumov.SudokuTest.Domain
{
    public class Row : Block
    {
        public override Cell this[int index]
        {
            set
            {
                base[index] = value;
                value.Row = this;
            }
        }
    }
}