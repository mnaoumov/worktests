using System;
using System.Collections.Generic;
using System.Linq;
using MichaelNaumov.SudokuTest.Strategies;

namespace MichaelNaumov.SudokuTest.Domain
{
    public class Sudoku : Block
    {
        readonly Row[] _rows;
        readonly Column[] _columns;
        readonly Group[] _groups;

        const int GroupSize = 3;
        public const int SudokuSize = GroupSize * GroupSize;
        const int CellsCount = SudokuSize * SudokuSize;

        SolveStrategy[] SolveStrategies { get; set; }

        public Sudoku()
        {
            _rows = new Row[SudokuSize];
            _columns = new Column[SudokuSize];
            _groups = new Group[SudokuSize];
            Cells = new Cell[CellsCount];
            InitializeModel();
            RegisterStrategies();
        }

        void RegisterStrategies()
        {
            //TODO Introduce more intelligent strategies
            SolveStrategies = new SolveStrategy[] { new SingleCandidateStrategy(), new HiddenSingleCandidateStrategy(), new BruteForceStrategy() };
            foreach (var solveStrategy in SolveStrategies)
                solveStrategy.Sudoku = this;
        }

        void InitializeModel()
        {
            for (int index = 0; index < SudokuSize; index++)
            {
                _rows[index] = new Row { Index = index, Sudoku = this };
                _columns[index] = new Column { Index = index, Sudoku = this };
                _groups[index] = new Group { Index = index, Sudoku = this };
            }

            for (int rowIndex = 0; rowIndex < SudokuSize; rowIndex++)
            {
                for (int columnIndex = 0; columnIndex < SudokuSize; columnIndex++)
                {
                    var cell = new Cell { Sudoku = this };

                    var row = _rows[rowIndex];
                    row[columnIndex] = cell;

                    var column = _columns[columnIndex];
                    column[rowIndex] = cell;

                    var cellInGroupIndex = GetCellInGroupIndex(rowIndex, columnIndex);
                    var group = GetGroup(rowIndex, columnIndex);
                    group[cellInGroupIndex] = cell;

                    var cellIndex = rowIndex * SudokuSize + columnIndex;
                    cell.Index = cellIndex;
                    Cells[cellIndex] = cell;
                }
            }
        }

        static int GetCellInGroupIndex(int rowIndex, int columnIndex)
        {
            int index = GetFlatIndex(rowIndex % GroupSize, columnIndex % GroupSize);
            return index;
        }

        Group GetGroup(int rowIndex, int columnIndex)
        {
            var groupIndex = GetFlatIndex(rowIndex / GroupSize, columnIndex / GroupSize);
            Group group = _groups[groupIndex];
            return group;
        }

        static int GetFlatIndex(int smallRowIndex, int smallColumnIndex)
        {
            int flatIndex = smallRowIndex * GroupSize + smallColumnIndex;
            return flatIndex;
        }

        public Cell GetCell(int rowIndex, int columnIndex)
        {
            var cell = _rows[rowIndex][columnIndex];
            return cell;
        }

        public void Solve()
        {
            Validate();
            PrepareCandidates();
            if (!SolveInternal())
                throw new ApplicationException("This sudoku is not solveable");
        }

        internal bool SolveInternal()
        {
            while (!Solved)
            {
                if (!CheckIfAllCellsHaveCandidates())
                    return false;
                var hasStrategy = SolveStrategies.Any(s => s.TryApply());
                if (!hasStrategy)
                    return false;
            }

            return true;
        }

        bool CheckIfAllCellsHaveCandidates()
        {
            var unsolvedCells = GetUnsolvedCells();
            var isValid = unsolvedCells.All(unsolvedCell => unsolvedCell.Candidates.Any());
            return isValid;
        }

        bool Solved
        {
            get
            {
                bool solved = !GetUnsolvedCells().Any();
                return solved;
            }
        }

        public override void PrepareCandidates()
        {
            foreach (var block in Blocks)
                block.PrepareCandidates();
            foreach (var cell in GetUnsolvedCells())
                cell.PrepareCandidates();
        }

        public override void Validate()
        {
            foreach (var block in Blocks)
                block.Validate();
        }

        public IEnumerable<Block> Blocks
        {
            get
            {
                foreach (var row in _rows)
                    yield return row;
                foreach (var column in _columns)
                    yield return column;
                foreach (var group in _groups)
                    yield return group;
            }
        }

        public override string ToString()
        {
            var str = string.Join(Environment.NewLine, _rows.Select(row => row.ToString()));
            return str;
        }

        public Sudoku Clone()
        {
            var sudoku = new Sudoku();
            sudoku.Load(this);
            return sudoku;
        }

        public void Load(Sudoku sudoku)
        {
            for (int index = 0; index < SudokuSize; index++)
            {
                _rows[index].Load(sudoku._rows[index]);
                _columns[index].Load(sudoku._columns[index]);
                _groups[index].Load(sudoku._groups[index]);
            }

            for (int cellIndex = 0; cellIndex < CellsCount; cellIndex++)
                Cells[cellIndex].Load(sudoku.Cells[cellIndex]);
        }
    }
}