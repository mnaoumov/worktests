using System.Collections.Generic;

namespace MichaelNaumov.SudokuTest.Domain
{
    public abstract class SudokuObjectBase
    {
        public Sudoku Sudoku { get; set; }
        public HashSet<int> Candidates { get; protected set; }
        public int Index { get; set; }
        public abstract void PrepareCandidates();
    }
}