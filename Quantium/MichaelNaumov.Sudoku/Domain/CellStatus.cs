namespace MichaelNaumov.SudokuTest.Domain
{
    public enum CellStatus
    {
        All,
        Solved,
        Unsolved
    }
}