namespace MichaelNaumov.SudokuTest.Domain
{
    public class Group : Block
    {
        public override Cell this[int index]
        {
            set
            {
                base[index] = value;
                value.Group = this;
            }
        }
    }
}
