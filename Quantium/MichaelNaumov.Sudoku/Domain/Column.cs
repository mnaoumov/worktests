namespace MichaelNaumov.SudokuTest.Domain
{
    public class Column : Block
    {
        public override Cell this[int index]
        {
            set
            {
                base[index] = value;
                value.Column = this;
            }
        }

    }
}