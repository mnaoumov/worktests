using System;
using System.Collections.Generic;
using System.Linq;

namespace MichaelNaumov.SudokuTest.Domain
{
    public abstract class Block : SudokuObjectBase
    {
        public Cell[] Cells { get; set; }

        protected Block()
        {
            Cells = new Cell[Sudoku.SudokuSize];
        }

        public virtual Cell this[int index]
        {
            get { return Cells[index]; }
            set { Cells[index] = value; }
        }

        public override string ToString()
        {
            string str = string.Format("| {0} |", string.Join(" | ", Cells.Select(c => c.ToString())));
            return str;
        }

        public virtual void Validate()
        {
            var blockValues = new HashSet<int>();

            foreach (var cell in GetCells(CellStatus.Solved))
            {
                if (!blockValues.Add((int) cell.Value))
                    throw new ApplicationException(string.Format("{0} #{1} is invalid. It contains duplicated value {2}.", BlockType, Index, cell.Value));
            }
        }

        public Cell[] GetCells(CellStatus cellStatus = CellStatus.All)
        {
            Cell[] cells;
            switch (cellStatus)
            {
                case CellStatus.All:
                    return Cells;
                case CellStatus.Solved:
                    cells = Cells.Where(cell => cell.Value != null).ToArray();
                    return cells;
                case CellStatus.Unsolved:
                    cells = Cells.Where(cell => cell.Value == null).ToArray();
                    return cells;
                default:
                    throw new NotSupportedException(string.Format("Invaliad cellStatus '{0}'.", cellStatus));
            }
        }

        public Cell[] GetUnsolvedCells()
        {
            return GetCells(CellStatus.Unsolved);
        }

        string BlockType
        {
            get
            {
                string blockType = GetType().Name;
                return blockType;
            }
        }

        public override void PrepareCandidates()
        {
            Candidates = new HashSet<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            foreach (var cell in GetCells(CellStatus.Solved))
                Candidates.Remove((int) cell.Value);
        }

        public void Load(Block block)
        {
            Candidates = new HashSet<int>(block.Candidates);
        }
    }
}