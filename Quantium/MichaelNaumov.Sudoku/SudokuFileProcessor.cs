﻿using System;
using System.IO;
using MichaelNaumov.SudokuTest.Domain;

namespace MichaelNaumov.SudokuTest
{
    public class SudokuFileProcessor
    {
        public Sudoku LoadFromFile(string inputFileName)
        {
            if (!File.Exists(inputFileName))
                throw new ApplicationException(String.Format("File {0} not exists.", inputFileName));

            var sudoku = new Sudoku();

            var fileContent = File.ReadAllText(inputFileName);

            var rowIndex = 0;
            var columnIndex = 0;

            foreach (char symbol in fileContent)
            {
                switch (symbol)
                {
                    case '|':
                    case ' ':
                        break;
                    case '.':
                        {
                            var cell = sudoku.GetCell(rowIndex, columnIndex);
                            cell.Value = null;
                            cell.ValueWasGiven = false;
                            columnIndex++;
                            break;

                        }
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        {
                            var cell = sudoku.GetCell(rowIndex, columnIndex);
                            cell.Value = symbol - '0';
                            cell.ValueWasGiven = true;
                            columnIndex++;
                            break;
                        }
                    case '\n':
                        columnIndex = 0;
                        rowIndex++;
                        break;
                }
            }
            return sudoku;
        }

        public void WriteToFile(string outputFileName, Sudoku sudoku)
        {
            File.WriteAllText(outputFileName, sudoku.ToString());
        }
    }
}