using System;
using System.Linq;

namespace MichaelNaumov.SudokuTest.Strategies
{
    /// <summary>
    /// This is very unefficient strategy. It heavily uses memory and potentially can cause <see cref="StackOverflowException">StackOverflowException</see>.
    /// It's better to introduce some more intelligent strategies instead.
    /// </summary>
    class BruteForceStrategy : SolveStrategy
    {
        public override bool TryApply()
        {
            var cell = Sudoku.GetUnsolvedCells().OrderBy(c => c.Candidates.Count).First();

            foreach (var candidate in cell.Candidates.ToArray())
            {
                var sudoku = Sudoku.Clone();
                sudoku.Cells[cell.Index].SetCalculatedValue(candidate);
                if (sudoku.SolveInternal())
                {
                    Sudoku.Load(sudoku);
                    return true;
                }
            }

            return false;
        }
    }
}