using System.Linq;

namespace MichaelNaumov.SudokuTest.Strategies
{
    /// <summary>
    /// This strategy covers the case when you have cell with only one candidate.
    /// </summary>
    class SingleCandidateStrategy : SolveStrategy
    {
        public override bool TryApply()
        {
            var cellWithSingleCandidate = Sudoku.GetUnsolvedCells().FirstOrDefault(c => c.Candidates.Count == 1);

            if (cellWithSingleCandidate == null)
                return false;

            cellWithSingleCandidate.SetCalculatedValue(cellWithSingleCandidate.Candidates.First());
            return true;
        }
    }
}