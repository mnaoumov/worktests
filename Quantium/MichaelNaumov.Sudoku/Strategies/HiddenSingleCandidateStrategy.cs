using System.Collections.Generic;
using System.Linq;
using MichaelNaumov.SudokuTest.Domain;

namespace MichaelNaumov.SudokuTest.Strategies
{
    /// <summary>
    /// This strategy covers the case when you have a group and one of the candidate values available only for one of the cells.
    /// It means that this value is necessary in that cell.
    /// </summary>
    class HiddenSingleCandidateStrategy : SolveStrategy
    {
        public override bool TryApply()
        {
            var isApplicable = Sudoku.Blocks.Any(TryApplyForBlock);
            return isApplicable;
        }

        static bool TryApplyForBlock(Block block)
        {
            var unsolvedCells = block.GetUnsolvedCells();
            foreach (var cell in unsolvedCells)
            {
                var otherCells = unsolvedCells.Except(new[] { cell }).ToArray();
                var otherCellCandidates = new HashSet<int>(otherCells.SelectMany(x => x.Candidates));
                var candidate = cell.Candidates.FirstOrDefault(c => !otherCellCandidates.Contains(c));
                if (candidate != 0)
                {
                    cell.SetCalculatedValue(candidate);
                    return true;
                }
            }

            return false;
        }
    }
}