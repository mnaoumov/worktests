using MichaelNaumov.SudokuTest.Domain;

namespace MichaelNaumov.SudokuTest.Strategies
{
    public abstract class SolveStrategy
    {
        public abstract bool TryApply();
        public Sudoku Sudoku { get; set; }
    }
}