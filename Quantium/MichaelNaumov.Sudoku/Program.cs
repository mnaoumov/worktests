﻿using System;
using System.IO;

namespace MichaelNaumov.SudokuTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //TODO: Nicer command-line parsing. E.g. Mono.Options.
            string inputFileName;
            if (args.Length >= 1)
                inputFileName = args[0];
            else
            {
                Console.Write("Please enter source file name: ");
                inputFileName = Console.ReadLine();
            }

            string outputFileName = args.Length >= 2 ? args[1] : GenerateOutputFileName(inputFileName);

            var sudokuFileProcessor = new SudokuFileProcessor();
            var sudoku = sudokuFileProcessor.LoadFromFile(inputFileName);
            sudoku.Solve();
            sudokuFileProcessor.WriteToFile(outputFileName, sudoku);
            Console.WriteLine(string.Format("Sudoku from file {0} is solved and result put in file {1}.", inputFileName, outputFileName));
            Console.ReadKey(true);
        }

        static string GenerateOutputFileName(string inputFileName)
        {
            var outputFileName = Path.Combine(Path.GetDirectoryName(inputFileName), string.Format("{0}_Solved{1}", Path.GetFileNameWithoutExtension(inputFileName), Path.GetExtension(inputFileName)));
            return outputFileName;
        }
    }
}
