﻿using System.Collections.Generic;
using System.IO;

namespace SDLTridion
{
    public static class FilesHelper
    {
        private static readonly List<string> xmlExtensions;

        static FilesHelper()
        {
            xmlExtensions = new List<string>(new string[] { ".xml", ".xslt", ".xsl" });
        }
        
        public static bool IsXmlFile(string filePath)
        {
            return xmlExtensions.Contains(Path.GetExtension(filePath));
        }

    }
}