﻿using System.Collections.Generic;
using System.IO;

namespace SDLTridion
{
    public class Task
    {
        private readonly string xmlPath;

        public Task(string xmlPath)
        {
            this.xmlPath = xmlPath;
        }

        public void Execute()
        {
            new List<string>(IterateFileNames()).ForEach(ProcessXmlFile);
        }

        private static void ProcessXmlFile(string xmlFileName)
        {
            new XmlReplacer(xmlFileName).Replace();
        }

        public IEnumerable<string> IterateFileNames()
        {
            foreach (string file in Directory.GetFiles(xmlPath))
            {
                if (FilesHelper.IsXmlFile(file))
                {
                    yield return file;
                }
            }
        }

    }
}