﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace SDLTridion
{
    public class XmlReplacer
    {
        private readonly string xmlFileName;

        public XmlReplacer(string xmlFileName)
        {
            this.xmlFileName = xmlFileName;
        }

        public void Replace()
        {
            CreateBackup();

            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.PreserveWhitespace = true;

            try
            {
                xmlDocument.Load(xmlFileName);
            }
            catch (XmlException e)
            {
                Console.WriteLine("An exception occured while processing file {0}\n{1}", xmlFileName, e.Message);

                return;
            }

            Replace(xmlDocument);

            SaveWithoutBom(xmlDocument);
        }

        private void SaveWithoutBom(XmlDocument xmlDocument)
        {
            using (TextWriter writer = new StreamWriter(xmlFileName, false, new UTF8Encoding(false)))
            {
                xmlDocument.Save(writer);
            }
        }

        private static void Replace(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return;
            }

            switch (xmlNode.NodeType)
            {
                case XmlNodeType.Text:
                case XmlNodeType.Comment:
                    xmlNode.Value = ReplaceText(xmlNode.Value);
                    return;
            }

            ReplaceChildren(xmlNode);

            ReplaceTitleAttribute(xmlNode);
        }

        private static void ReplaceTitleAttribute(XmlNode xmlNode)
        {
            XmlAttributeCollection attributes = xmlNode.Attributes;

            if (attributes != null)
            {
                Replace(attributes["title"]);
            }
        }

        private static void ReplaceChildren(XmlNode xmlNode)
        {
            foreach (XmlNode childNode in xmlNode.ChildNodes)
            {
                Replace(childNode);
            }
        }

        private void CreateBackup()
        {
            string backupFileName = string.Format("{0}.bak", xmlFileName);

            File.Copy(xmlFileName, backupFileName, true);
        }

        private static string ReplaceText(string text)
        {
            Regex regex = new Regex("(?<!SDL )Tridion");

            return regex.Replace(text, "SDL Tridion");
        }
    }
}