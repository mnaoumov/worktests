using System.Collections.Generic;
using System.IO;
using System.Xml;
using NUnit.Framework;

namespace SDLTridion
{
    [TestFixture]
    public class UnitTests
    {
        private const string testsFolder = @"c:\Test\SDLTridion\SDLTridion\SDLTridion\UnitTests";

        [Test]
        public void TestFiles()
        {
            string testFolder = CreateUnitTestFolder("TestFiles");

            string[] fileNames = new string[]
                                     {
                                         "1.xml", "2.xma", "3.xslt", "4.xsl", "5.xxx", "6", "7.asass", "aa.xxx.xml",
                                         "aaa.xmlt", "asasas.xxx.xsl"
                                     };

            foreach (string fileName in fileNames)
            {
                File.Create(string.Format(@"{0}\{1}", testFolder, fileName));
            }

            Task task = new Task(testFolder);

            List<string> files = new List<string>(task.IterateFileNames());

            Assert.AreEqual(files.Count, 5);
        }

        [Test]
        public void TestText()
        {
            string testFolder = CreateUnitTestFolder("TestText");

            string testXml = string.Format(@"{0}\test.xml", testFolder);

            using (StreamWriter writer = new StreamWriter(testXml))
            {
                writer.Write(@"<?xml version=""1.0"" encoding=""utf-8""?>
<text>SDL Tridion should not be replaced
Tridion should be replaced</text>");
            }

            new XmlReplacer(testXml).Replace();

            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.Load(testXml);

            string value = xmlDocument.SelectSingleNode("text").InnerText;

            Assert.AreEqual(value, @"SDL Tridion should not be replaced
SDL Tridion should be replaced");
        }

        [Test]
        public void TestTitleAttribute()
        {
            string testFolder = CreateUnitTestFolder("TestTitleAttribute");

            string testXml = string.Format(@"{0}\test.xml", testFolder);

            using (StreamWriter writer = new StreamWriter(testXml))
            {
                writer.Write(
                    @"<?xml version=""1.0"" encoding=""utf-8""?>
<text notReplace=""Tridion should not be replaced"" title=""Tridion should be replaced"" />");
            }

            new XmlReplacer(testXml).Replace();

            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.Load(testXml);

            XmlElement textNode = (XmlElement) xmlDocument.SelectSingleNode("text");

            Assert.AreEqual(textNode.GetAttribute("notReplace"), @"Tridion should not be replaced");
            Assert.AreEqual(textNode.GetAttribute("title"), @"SDL Tridion should be replaced");
        }

        [Test]
        public void TestComment()
        {
            string testFolder = CreateUnitTestFolder("TestComment");

            string testXml = string.Format(@"{0}\test.xml", testFolder);

            using (StreamWriter writer = new StreamWriter(testXml))
            {
                writer.Write(
                    @"<?xml version=""1.0"" encoding=""utf-8""?>
<!-- comment should not be replaced SDL Tridion -->
<text />
<!-- comment should be replaced Tridion -->");
            }

            new XmlReplacer(testXml).Replace();

            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.PreserveWhitespace = true;

            xmlDocument.Load(testXml);

            Assert.AreEqual(xmlDocument.OuterXml,
                            @"<?xml version=""1.0"" encoding=""utf-8""?>
<!-- comment should not be replaced SDL Tridion -->
<text />
<!-- comment should be replaced SDL Tridion -->");
        }


        private static string CreateUnitTestFolder(string folderName)
        {
            string folderPath = string.Format(@"{0}\{1}", testsFolder, folderName);

            if (Directory.Exists(folderPath))
            {
                DeleteFolder(folderPath);
            }

            Directory.CreateDirectory(folderPath);

            return folderPath;
        }

        private static void DeleteFolder(string folderPath)
        {
            foreach (string file in Directory.GetFiles(folderPath))
            {
                File.Delete(file);
            }

            Directory.Delete(folderPath);
        }
    }
}