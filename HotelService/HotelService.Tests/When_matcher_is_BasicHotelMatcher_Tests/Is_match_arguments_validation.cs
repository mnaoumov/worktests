﻿using System;
using HotelService.Domain;
using NUnit.Framework;

namespace HotelService.Tests.When_matcher_is_BasicHotelMatcher_Tests
{
    [TestFixture]
    public class Is_match_arguments_validation : When_matcher_is_BasicHotelMatcher
    {
        [Test]
        public void If_supplier_hotel_is_null_ArgumentNullException_thrown()
        {
            SupplierHotel supplierHotel = null;
            Hotel hotel = new Hotel();
            Assert.That(() => _matcher.IsMatch(supplierHotel, hotel), Throws.TypeOf<ArgumentNullException>());
        }

        [Test]
        public void If_hotel_is_null_ArgumentNullException_thrown()
        {
            SupplierHotel supplierHotel = new SupplierHotel();
            Hotel hotel = null;
            Assert.That(() => _matcher.IsMatch(supplierHotel, hotel), Throws.TypeOf<ArgumentNullException>());
        }

    }
}