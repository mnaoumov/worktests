using NUnit.Framework;

namespace HotelService.Tests.When_matcher_is_BasicHotelMatcher_Tests
{
    public abstract class When_matcher_is_BasicHotelMatcher
    {
        protected BasicHotelMatcher _matcher;

        [SetUp]
        public void SetUp()
        {
            InitBasicHotelMatcher();
        }

        void InitBasicHotelMatcher()
        {
            _matcher = new BasicHotelMatcher();
        }
    }
}