using System;
using NUnit.Framework;

namespace HotelService.Tests.When_matcher_is_BasicHotelMatcher_Tests.IsMatch_for_a_specific_supplier_Tests
{
    public abstract class IsMatch_for_a_specific_supplier : When_matcher_is_BasicHotelMatcher
    {
        protected string _supplierCode;

        [SetUp]
        public void SetUp()
        {
            base.SetUp();
            InitSupplierCode();
        }

        protected abstract void InitSupplierCode();

        protected abstract class BaseIsMatchTestCaseData : ITestCaseData
        {
            public bool IsMatch { get; set; }
            public abstract object[] Arguments { get; }
            public object Result { get { return IsMatch; } }
            public Type ExpectedException { get; set; }
            public string ExpectedExceptionName { get; set; }
            public string TestName { get; set; }
            public string Description { get; set; }
            public bool Ignored { get; set; }
            public string IgnoreReason { get; set; }
        }
    }
}