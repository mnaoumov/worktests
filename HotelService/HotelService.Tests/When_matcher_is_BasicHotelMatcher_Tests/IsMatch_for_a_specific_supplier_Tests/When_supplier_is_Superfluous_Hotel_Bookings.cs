using HotelService.Domain;
using NUnit.Framework;

namespace HotelService.Tests.When_matcher_is_BasicHotelMatcher_Tests.IsMatch_for_a_specific_supplier_Tests
{
    [TestFixture]
    public class When_supplier_is_Superfluous_Hotel_Bookings : IsMatch_for_a_specific_supplier
    {
        protected override void InitSupplierCode()
        {
            _supplierCode = SupplierCodes.SuperfluousHotelBookings;
        }

        [Test]
        [TestCaseSource("IsMatchTestCases")]
        public bool Supplier_hotel_matches_our_hotel(string supplierHotelName, string supplierHotelAddress, string hotelName, string hotelAddress)
        {
            SupplierHotel supplierHotel = CreateSupplierHotel(
                name: supplierHotelName,
                address: supplierHotelAddress);
            var hotel = new Hotel
                            {
                                Name = hotelName,
                                Address = hotelAddress
                            };

            return _matcher.IsMatch(supplierHotel, hotel);
        }

        SupplierHotel CreateSupplierHotel(string name, string address)
        {
            return new SupplierHotel
                       {
                           SupplierCode = _supplierCode,
                           Name = name,
                           Address = address,
                       };
        }

        IsMatchTestCaseData[] IsMatchTestCases
        {
            get
            {
                return new[]
                           {
                               new IsMatchTestCaseData
                                   {
                                       TestName = "[Specification example] Match if both the hotel name and address match when punctuation is excluded",
                                       SupplierHotelName = "*Good*-Times! HOTEL (Dubbo)",
                                       SupplierHotelAddress = "22 Carrington Square, Dubbo NSW",
                                       HotelName = "Good Times Hotel, Dubbo",
                                       HotelAddress = "22 Carrington Square, Dubbo NSW",
                                       IsMatch = true
                                   },
                               new IsMatchTestCaseData
                                   {
                                       TestName = "Match if name and address fully match",
                                       SupplierHotelName = "*Good*-Times! HOTEL (Dubbo)",
                                       SupplierHotelAddress = "22 Carrington Square, Dubbo NSW",
                                       HotelName = "*Good*-Times! HOTEL (Dubbo)",
                                       HotelAddress = "22 Carrington Square, Dubbo NSW",
                                       IsMatch = true
                                   },
                               new IsMatchTestCaseData
                                   {
                                       TestName = "Is not match if address is very different",
                                       SupplierHotelName = "*Good*-Times! HOTEL (Dubbo)",
                                       SupplierHotelAddress = "22 Carrington Square, Dubbo NSW",
                                       HotelName = "*Good*-Times! HOTEL (Dubbo)",
                                       HotelAddress = "Absolutely another address",
                                       IsMatch = false
                                   },
                               new IsMatchTestCaseData
                                   {
                                       TestName = "Is not match if name is very different",
                                       SupplierHotelName = "*Good*-Times! HOTEL (Dubbo)",
                                       SupplierHotelAddress = "22 Carrington Square, Dubbo NSW",
                                       HotelName = "Absolutely another name",
                                       HotelAddress = "22 Carrington Square, Dubbo NSW",
                                       IsMatch = false
                                   },
                               new IsMatchTestCaseData
                                   {
                                       TestName = "Is not match if name and address differ only by case",
                                       SupplierHotelName = "*Good*-Times! HOTEL (Dubbo)",
                                       SupplierHotelAddress = "22 Carrington Square, Dubbo NSW",
                                       HotelName = "*Good*-TIMES! HOTEL (DuBbo)",
                                       HotelAddress = "22 Carrington Square, dubbo nsW",
                                       IsMatch = true
                                   },
                               new IsMatchTestCaseData
                                   {
                                       TestName = "Match if name and address differ only by punctuation",
                                       SupplierHotelName = "*Good*-Times! HOTEL (Dubbo)",
                                       SupplierHotelAddress = "22 Carrington-Square, Dubbo, NSW.",
                                       HotelName = "*Good*-Times! HOTEL (Dubbo)...",
                                       HotelAddress = "22:::!!! Carrington-----Square,!, Dubbo, NSW.",
                                       IsMatch = true
                                   },
                               new IsMatchTestCaseData
                                   {
                                       TestName = "Match if name and address differ only by whitespaces",
                                       SupplierHotelName = "*Good*-Times! HOTEL (Dubbo)",
                                       SupplierHotelAddress = "22 Carrington \nSquare,\r Dubbo NSW",
                                       HotelName = "*Good*-Times!         HOTEL (Dubbo)",
                                       HotelAddress = "22 Carrington \t Square, Dubbo          NSW",
                                       IsMatch = true
                                   }
                           };
            }
        }

        class IsMatchTestCaseData : BaseIsMatchTestCaseData
        {
            public string SupplierHotelName { get; set; }
            public string SupplierHotelAddress { get; set; }
            public string HotelName { get; set; }
            public string HotelAddress { get; set; }
            public override object[] Arguments
            {
                get { return new[] { SupplierHotelName, SupplierHotelAddress, HotelName, HotelAddress }; }
            }
        }
    }
}