using HotelService.Domain;
using NUnit.Framework;

namespace HotelService.Tests.When_matcher_is_BasicHotelMatcher_Tests.IsMatch_for_a_specific_supplier_Tests
{
    [TestFixture]
    public class When_supplier_is_Thereabouts_Holidays : IsMatch_for_a_specific_supplier
    {
        protected override void InitSupplierCode()
        {
            _supplierCode = SupplierCodes.ThereaboutsHolidays;
        }

        [Test]
        [TestCaseSource("IsMatchTestCases")]
        public bool Supplier_hotel_matches_our_hotel(string supplierHotelChainCode, decimal supplierHotelLatitude, decimal supplierHotelLongitude, string hotelChainCode, decimal hotelLatitude, decimal hotelLongitude)
        {
            SupplierHotel supplierHotel = CreateSupplierHotel(supplierHotelChainCode, supplierHotelLatitude, supplierHotelLongitude);
            var hotel = new Hotel
                            {
                                ChainCode = hotelChainCode,
                                Latitude = hotelLatitude,
                                Longitude = hotelLongitude
                            };

            return _matcher.IsMatch(supplierHotel, hotel);
        }

        SupplierHotel CreateSupplierHotel(string chainCode, decimal latitude, decimal longitude)
        {
            return new SupplierHotel
                       {
                           SupplierCode = _supplierCode,
                           ChainCode = chainCode,
                           Latitude = latitude,
                           Longitude = longitude
                       };
        }

        IsMatchTestCaseData[] IsMatchTestCases
        {
            get
            {
                return new[]
                           {
                               new IsMatchTestCaseData
                                   {
                                       TestName = "Match if chain code and location match",
                                       SupplierHotelChainCode = "ChainCode123",
                                       SupplierHotelLatitude = 12.345m,
                                       SupplierHotelLongitude = 67.890m,
                                       HotelChainCode = "ChainCode123",
                                       HotelLatitude = 12.345m,
                                       HotelLongitude = 67.890m,
                                       IsMatch = true
                                   },
                                   new IsMatchTestCaseData
                                   {
                                       TestName = "Is not match if chain codes differ",
                                       SupplierHotelChainCode = "ChainCode123",
                                       SupplierHotelLatitude = 12.345m,
                                       SupplierHotelLongitude = 67.890m,
                                       HotelChainCode = "DifferentChainCode",
                                       HotelLatitude = 12.345m,
                                       HotelLongitude = 67.890m,
                                       IsMatch = false
                                   },
                                   new IsMatchTestCaseData
                                   {
                                       TestName = "Match if location is slightly different",
                                       SupplierHotelChainCode = "ChainCode123",
                                       SupplierHotelLatitude = 12.345123123m,
                                       SupplierHotelLongitude = 67.890890890m,
                                       HotelChainCode = "ChainCode123",
                                       HotelLatitude = 12.345123127m,
                                       HotelLongitude = 67.890890886m,
                                       IsMatch = true
                                   },
                                   new IsMatchTestCaseData
                                   {
                                       TestName = "Is not match if location is significantly different",
                                       SupplierHotelChainCode = "ChainCode123",
                                       SupplierHotelLatitude = 12.345m,
                                       SupplierHotelLongitude = 67.890m,
                                       HotelChainCode = "ChainCode123",
                                       HotelLatitude = 10.346m,
                                       HotelLongitude = 70.888m,
                                       IsMatch = false
                                   },
                                   new IsMatchTestCaseData
                                   {
                                       TestName = "Match if location differs for 200 meters",
                                       SupplierHotelChainCode = "ChainCode123",
                                       SupplierHotelLatitude = 12.345m,
                                       SupplierHotelLongitude = 67.890m,
                                       HotelChainCode = "ChainCode123",
                                       HotelLatitude = 12.345m + Distance.FromMetres(200).InDegrees(),
                                       HotelLongitude = 67.890m,
                                       IsMatch = true
                                   },
                                   new IsMatchTestCaseData
                                   {
                                       TestName = "Match if location differs for 199 meters",
                                       SupplierHotelChainCode = "ChainCode123",
                                       SupplierHotelLatitude = 12.345m,
                                       SupplierHotelLongitude = 67.890m,
                                       HotelChainCode = "ChainCode123",
                                       HotelLatitude = 12.345m + Distance.FromMetres(199).InDegrees(),
                                       HotelLongitude = 67.890m,
                                       IsMatch = true
                                   },
                                   new IsMatchTestCaseData
                                   {
                                       TestName = "Is not match if location differs for 201 meters",
                                       SupplierHotelChainCode = "ChainCode123",
                                       SupplierHotelLatitude = 12.345m,
                                       SupplierHotelLongitude = 67.890m,
                                       HotelChainCode = "ChainCode123",
                                       HotelLatitude = 12.345m + Distance.FromMetres(201).InDegrees(),
                                       HotelLongitude = 67.890m,
                                       IsMatch = false
                                   },
                           };
            }
        }

        class IsMatchTestCaseData : BaseIsMatchTestCaseData
        {
            public string SupplierHotelChainCode { get; set; }
            public decimal SupplierHotelLatitude { get; set; }
            public decimal SupplierHotelLongitude { get; set; }
            public string HotelChainCode { get; set; }
            public decimal HotelLatitude { get; set; }
            public decimal HotelLongitude { get; set; }
            public override object[] Arguments
            {
                get { return new object[] { SupplierHotelChainCode, SupplierHotelLatitude, SupplierHotelLongitude, HotelChainCode, HotelLatitude, HotelLongitude }; }
            }
        }
    }
}