using HotelService.Domain;
using NUnit.Framework;

namespace HotelService.Tests.When_matcher_is_BasicHotelMatcher_Tests.IsMatch_for_a_specific_supplier_Tests
{
    [TestFixture]
    public class When_supplier_is_Contrary_Country_Getaways : IsMatch_for_a_specific_supplier
    {
        protected override void InitSupplierCode()
        {
            _supplierCode = SupplierCodes.ContraryCountryGetaways;
        }

        [Test]
        [TestCaseSource("IsMatchTestCases")]
        public bool Supplier_hotel_matches_our_hotel(string supplierHotelName, string hotelName)
        {
            SupplierHotel supplierHotel = CreateSupplierHotel(supplierHotelName);
            var hotel = new Hotel
                            {
                                Name = hotelName
                            };

            return _matcher.IsMatch(supplierHotel, hotel);
        }

        SupplierHotel CreateSupplierHotel(string name)
        {
            return new SupplierHotel
                       {
                           SupplierCode = _supplierCode,
                           Name = name
                       };
        }

        IsMatchTestCaseData[] IsMatchTestCases
        {
            get
            {
                return new[]
                           {
                               new IsMatchTestCaseData
                                   {
                                       TestName = "[Specification example] Match if the names match when the words in the name of the Contrary hotel are reversed",
                                       SupplierHotelName = "Manor Country Mayhew�s Ralph Lord",
                                       HotelName = "Lord Ralph Mayhew�s Country Manor",
                                       IsMatch = true
                                   },
                                   new IsMatchTestCaseData
                                   {
                                       TestName = "Match if name is the same one word",
                                       SupplierHotelName = "BestHotel",
                                       HotelName = "BestHotel",
                                       IsMatch = true
                                   },
                                   new IsMatchTestCaseData
                                   {
                                       TestName = "Is not match if name is not the same one word",
                                       SupplierHotelName = "BestHotel",
                                       HotelName = "WorstHotel",
                                       IsMatch = false
                                   },
                                   new IsMatchTestCaseData
                                   {
                                       TestName = "Match if name is in two words and they swapped",
                                       SupplierHotelName = "Best Hotel",
                                       HotelName = "Hotel Best",
                                       IsMatch = true
                                   },
                           };
            }
        }

        class IsMatchTestCaseData : BaseIsMatchTestCaseData
        {
            public string SupplierHotelName { get; set; }
            public string HotelName { get; set; }
            public override object[] Arguments
            {
                get { return new object[] { SupplierHotelName, HotelName }; }
            }
        }
    }
}