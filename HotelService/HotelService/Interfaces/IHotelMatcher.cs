﻿using HotelService.Domain;

namespace HotelService.Interfaces
{
    public interface IHotelMatcher
    {
        bool IsMatch(SupplierHotel supplierHotel, Hotel hotel);
    }
}