namespace HotelService
{
    public static class SupplierCodes
    {
        public const string SuperfluousHotelBookings = "SUP";
        public const string ThereaboutsHolidays = "HUH";
        public const string ContraryCountryGetaways = "GCC";
    }
}