﻿using HotelService.Domain;

namespace HotelService.SupplierMatchers
{
    class ThereaboutsHolidaysMatcher : BaseSupplierHotelMatcher
    {
        static readonly Distance _allowedDistance = Distance.FromMetres(200);

        protected override string MatcherSupplierCode
        {
            get { return SupplierCodes.ThereaboutsHolidays; }
        }

        protected override bool SupplierHotelIsMatch(SupplierHotel supplierHotel, Hotel hotel)
        {
            return supplierHotel.ChainCode == hotel.ChainCode && AreVeryClose(supplierHotel, hotel);
        }

        static bool AreVeryClose(SupplierHotel supplierHotel, Hotel hotel)
        {
            var distanceInMeters = GetDistanceBetween(supplierHotel, hotel);
            return distanceInMeters <= _allowedDistance;
        }

        static Distance GetDistanceBetween(SupplierHotel supplierHotel, Hotel hotel)
        {
            var supplierHotelLocation = new Location(supplierHotel.Latitude, supplierHotel.Longitude);
            var hotelLocation = new Location(hotel.Latitude, hotel.Longitude);
            return supplierHotelLocation.CalculateDistanceTo(hotelLocation);
        }
    }
}