using System;
using HotelService.Domain;
using HotelService.Interfaces;

namespace HotelService.SupplierMatchers
{
    abstract class BaseSupplierHotelMatcher : IHotelMatcher
    {
        public virtual bool IsMatch(SupplierHotel supplierHotel, Hotel hotel)
        {
            if (supplierHotel.SupplierCode != MatcherSupplierCode)
            {
                throw new ApplicationException(string.Format("HotelMatcher {0} cannot match supplier hotel with code {1}.", GetType(), supplierHotel.SupplierCode));
            }

            return SupplierHotelIsMatch(supplierHotel, hotel);
        }

        protected abstract string MatcherSupplierCode { get; }

        protected abstract bool SupplierHotelIsMatch(SupplierHotel supplierHotel, Hotel hotel);
    }
}