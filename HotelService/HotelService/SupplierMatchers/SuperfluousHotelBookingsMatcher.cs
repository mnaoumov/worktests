using System;
using System.Linq;
using System.Text.RegularExpressions;
using HotelService.Domain;

namespace HotelService.SupplierMatchers
{
    class SuperfluousHotelBookingsMatcher : BaseSupplierHotelMatcher
    {
        protected override string MatcherSupplierCode
        {
            get { return SupplierCodes.SuperfluousHotelBookings; }
        }

        protected override bool SupplierHotelIsMatch(SupplierHotel supplierHotel, Hotel hotel)
        {
            return IsMatch(supplierHotel.Name, hotel.Name) && IsMatch(supplierHotel.Address, hotel.Address);
        }

        static bool IsMatch(string str1, string str2)
        {
            string strWithoutPunctuation1 = RemovePunctuation(str1);
            string strWithoutPunctuation2 = RemovePunctuation(str2);
            return StringComparer.InvariantCultureIgnoreCase.Equals(strWithoutPunctuation1, strWithoutPunctuation2);
        }

        static string RemovePunctuation(string str)
        {
            const string punctuationAndWhitespaceRegex = @"(?:\p{P}|\s)+";
            string[] words = Regex.Split(str, punctuationAndWhitespaceRegex).Where(s => !String.IsNullOrEmpty(s)).ToArray();
            return String.Join(" ", words);
        }
    }
}