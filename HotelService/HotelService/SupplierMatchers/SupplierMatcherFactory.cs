using System;
using HotelService.Interfaces;

namespace HotelService.SupplierMatchers
{
    static class SupplierMatcherFactory
    {
        public static IHotelMatcher CreateByCode(string supplierCode)
        {
            switch (supplierCode)
            {
                case SupplierCodes.SuperfluousHotelBookings:
                    return new SuperfluousHotelBookingsMatcher();
                case SupplierCodes.ThereaboutsHolidays:
                    return new ThereaboutsHolidaysMatcher();
                case SupplierCodes.ContraryCountryGetaways:
                    return new ContraryCountryGetawaysMatcher();
                default:
                    throw new NotSupportedException();
            }
        }
    }
}