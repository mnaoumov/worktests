﻿using HotelService.Domain;
using System.Linq;

namespace HotelService.SupplierMatchers
{
    class ContraryCountryGetawaysMatcher : BaseSupplierHotelMatcher
    {
        protected override string MatcherSupplierCode
        {
            get { return SupplierCodes.ContraryCountryGetaways; }
        }

        protected override bool SupplierHotelIsMatch(SupplierHotel supplierHotel, Hotel hotel)
        {
            var supplierHotelSwappedName = ReverseAllWords(supplierHotel.Name);
            return supplierHotelSwappedName == hotel.Name;
        }

        static string ReverseAllWords(string str)
        {
            string[] words = str.Split(' ');
            return string.Join(" ", words.Reverse());
        }
    }
}