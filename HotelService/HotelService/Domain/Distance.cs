using System;

namespace HotelService.Domain
{
    public class Distance
    {
        const decimal MetresInKilometre = 1000m;
        const decimal KilometresInDegree = 111m;

        decimal Metres { get; set; }

        Distance(decimal metres)
        {
            Metres = metres;
        }

        public static Distance FromMetres(decimal metres)
        {
            return new Distance(metres);
        }

        public static bool operator <=(Distance distance1, Distance distance2)
        {
            return Decimal.Compare(distance1.Metres, distance2.Metres) <= 0;
        }

        public static bool operator >=(Distance distance1, Distance distance2)
        {
            return Decimal.Compare(distance1.Metres, distance2.Metres) >= 0;
        }

        public static Distance FromKilometres(decimal kilometres)
        {
            return FromMetres(kilometres * MetresInKilometre);
        }

        public static Distance FromDegrees(decimal degrees)
        {
            return FromKilometres(degrees * KilometresInDegree);
        }

        public decimal InDegrees()
        {
            return (Metres / MetresInKilometre) / KilometresInDegree;
        }
    }
}