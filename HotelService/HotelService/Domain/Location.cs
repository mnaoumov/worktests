using System;

namespace HotelService.Domain
{
    class Location
    {
        decimal Latitude { get; set; }
        decimal Longitude { get; set; }

        public Location(decimal latitude, decimal longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public Distance CalculateDistanceTo(Location other)
        {
            var latitudeDiff = other.Latitude - Latitude;
            var longitudeDiff = other.Longitude - Longitude;
            var distanceInDegrees = (decimal)Math.Sqrt((double)(latitudeDiff * latitudeDiff + longitudeDiff * longitudeDiff));
            return Distance.FromDegrees(distanceInDegrees);
        }
    }
}