using System;
using HotelService.Domain;
using HotelService.Interfaces;
using HotelService.SupplierMatchers;

namespace HotelService
{
    public class BasicHotelMatcher : IHotelMatcher
    {
        public bool IsMatch(SupplierHotel supplierHotel, Hotel hotel)
        {
            if (supplierHotel == null)
                throw new ArgumentNullException("supplierHotel");
            if (hotel == null)
                throw new ArgumentNullException("hotel");

            var matcher = SupplierMatcherFactory.CreateByCode(supplierHotel.SupplierCode);
            return matcher.IsMatch(supplierHotel, hotel);
        }
    }
}